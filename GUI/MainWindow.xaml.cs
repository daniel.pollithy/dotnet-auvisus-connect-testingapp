﻿using AuvisusWebsocketClient;
using System;
using System.Windows;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        private AuvisusWebsocketClient.AuvisusWebsocketClient websocketClient;
        private string websocketAddress = "ws://localhost:6065";
        public MainWindow()
        {
            InitializeComponent();
        }
        private void startWebsocketClient()
        {
            websocketClient = new AuvisusWebsocketClient.AuvisusWebsocketClient(
                websocketAddress,
                this.OnMessageHandler,
                this.OnErrorHandler,
                this.OnConnectHandler
            );
            websocketClient.Start();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            // Get ws url
            websocketAddress = wsAddress.Text;
            // disable the textbox
            wsAddress.IsEnabled = false;

            // start looping server thread
            startWebsocketClient();

            // delete start connection button
            startConnection.Visibility = Visibility.Hidden;
        }

        public void OnMessageHandler(string message)
        {
            Dispatcher.Invoke(new Action(() => { incomingText.Text += message; }));
        }

        public void OnErrorHandler(string message)
        {
            Dispatcher.Invoke(new Action(() => { connectionStatusLabel.Content = message; }));
        }

        public void OnConnectHandler(string message)
        {
            Dispatcher.Invoke(new Action(() => { connectionStatusLabel.Content = message; }));
            MessageBox.Show(message);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            commandTextBox.Text = "{\"type\":\"articles_synced\",\"articles\":[{\"id\":\"1\",\"name\":{\"DE\":{\"short\":\"Maultaschen\",\"addOn\":\"mit Zwiebelschm\u00e4lze\"}},\"price\":{\"intern\":3.25}},{\"id\":\"2\",\"name\":{\"DE\":{\"short\":\"Spaghetti Arrabiata\",\"addOn\":\"scharf\"}},\"price\":{\"intern\":2.95}},{\"id\":\"3\",\"name\":{\"DE\":{\"short\":\"Pommes\",\"addOn\":\"klein\"}},\"price\":{\"intern\":1.2}},{\"id\":\"4\",\"name\":{\"DE\":{\"short\":\"Beilagensalat\",\"addOn\":\"mit Dressing\"}},\"price\":{\"intern\":0.8}},{\"id\":\"5\",\"name\":{\"DE\":{\"short\":\"Reis\",\"addOn\":\"klein\"}},\"price\":{\"intern\":1.1}},{\"id\":\"6\",\"name\":{\"DE\":{\"short\":\"Erdbeerquark\",\"addOn\":\"klein\"}},\"price\":{\"intern\":0.7}},{\"id\":\"7\",\"name\":{\"DE\":{\"short\":\"Br\u00f6tchen\",\"addOn\":\"Tafelbr\u00f6tchen\"}},\"price\":{\"intern\":0.6}},{\"id\":\"8\",\"name\":{\"DE\":{\"short\":\"Orange\",\"addOn\":\"Frisch in Bioqualit\u00e4t\"}},\"price\":{\"intern\":0.6}},{\"id\":\"9\",\"name\":{\"DE\":{\"short\":\"Banane\",\"addOn\":\"Frisch in Bioqualit\u00e4t\"}},\"price\":{\"intern\":0.6}},{\"id\":\"10\",\"name\":{\"DE\":{\"short\":\"Apfel\",\"addOn\":\"Frisch und regional\"}},\"price\":{\"intern\":0.6}},{\"id\":\"11\",\"name\":{\"DE\":{\"short\":\"lemonaid\",\"addOn\":\"330ml\"}},\"price\":{\"intern\":1.1}},{\"id\":\"12\",\"name\":{\"DE\":{\"short\":\"Snickers\",\"addOn\":\"Schokoriegel\"}},\"price\":{\"intern\":0.8}},{\"id\":\"13\",\"name\":{\"DE\":{\"short\":\"Demo-Hauptgericht 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":2.8}},{\"id\":\"14\",\"name\":{\"DE\":{\"short\":\"Demo-Hauptgericht 2\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":3.8}},{\"id\":\"15\",\"name\":{\"DE\":{\"short\":\"Demo-Beilage 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.8}},{\"id\":\"16\",\"name\":{\"DE\":{\"short\":\"Demo-Beilage 2\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":1.2}},{\"id\":\"17\",\"name\":{\"DE\":{\"short\":\"Demo-Dessert 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.9}},{\"id\":\"18\",\"name\":{\"DE\":{\"short\":\"Demo-Dessert 2\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":1.3}},{\"id\":\"19\",\"name\":{\"DE\":{\"short\":\"Demo-Getr\u00e4nk 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":1.2}},{\"id\":\"20\",\"name\":{\"DE\":{\"short\":\"Demo-Getr\u00e4nk 2\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":1.8}},{\"id\":\"21\",\"name\":{\"DE\":{\"short\":\"Demo-Sonstiges 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.6}},{\"id\":\"22\",\"name\":{\"DE\":{\"short\":\"Demo-Sonstiges 2\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.7}},{\"id\":\"23\",\"name\":{\"DE\":{\"short\":\"Kleiner Salat\",\"addOn\":\"vom Buffet\"}},\"price\":{\"intern\":1}},{\"id\":\"24\",\"name\":{\"DE\":{\"short\":\"Mittlerer Salat\",\"addOn\":\"vom Buffet\"}},\"price\":{\"intern\":2}},{\"id\":\"25\",\"name\":{\"DE\":{\"short\":\"Gro\u00dfer Salat\",\"addOn\":\"vom Buffet\"}},\"price\":{\"intern\":3}}]}";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            commandTextBox.Text = "{\n\"type\": \"card_placed\", \"cardId\": \"a4157eab-2aac-4740-ad2a-158014a56a42\"}";
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (websocketClient != null)
            {
                websocketClient.SendExternal(commandTextBox.Text);
                outgoingText.Text += commandTextBox.Text;
                outgoingText.Text += "\n";
            } else
            {
                MessageBox.Show("Start a connection first");
            }
            
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            commandTextBox.Text = "{\"type\":\"transaction_processed\",\"transaction\":{\"transactionResult\":\"unknownArticle\",\"auvisusTransactionId\":\"d1b1a822-e534-4514-b8e2-bbba3209ba47\",\"transactionId\":\"8db6351d-d4ff-4a47-a52a-f663ee317c59\",\"priceLevel\":\"intern\",\"totalCost\":5.32,\"funds\":2.32,\"basket\":[{\"id\":\"21\",\"name\":{\"DE\":{\"short\":\"Demo-Sonstiges 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.6}},{\"id\":\"23\",\"name\":{\"DE\":{\"short\":\"Kleiner Salat\",\"addOn\":\"vom Buffet\"}},\"price\":{\"intern\":1}},{\"id\":\"6\",\"name\":{\"DE\":{\"short\":\"Erdbeerquark\",\"addOn\":\"klein\"}},\"price\":{\"intern\":0.7}},{\"id\":\"15\",\"name\":{\"DE\":{\"short\":\"Demo-Beilage 1\",\"addOn\":\"Live in Demo eintrainiert\"}},\"price\":{\"intern\":0.8}},{\"id\":\"10\",\"name\":{\"DE\":{\"short\":\"Apfel\",\"addOn\":\"Frisch und regional\"}},\"price\":{\"intern\":0.6}}],\"cardId\":\"cc84567b-aed9-4208-8395-44e75f50710e\"}}";
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            outgoingText.Text = "";
            incomingText.Text = "";
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            commandTextBox.Text = "{\"type\":\"payment_methods_synced\",\"paymentMethods\":[{\"id\":\"1\",\"name\":\"Key Card\",\"triggerOnCardPlaced\":true},{\"id\":\"2\",\"name\":\"Online Wallet\",\"triggerOnCardPlaced\":true},{\"id\":\"3\",\"name\":\"EC\",\"triggerOnCardPlaced\":false}],\"defaultPaymentMethodId\":\"1\"}";
        }

        
    }
}
