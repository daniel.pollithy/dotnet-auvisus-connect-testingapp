﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net.WebSockets;
using System.Windows;

namespace AuvisusWebsocketClient
{
    public delegate void OnWSConnectCallback(string connectionMessage);
    public delegate void OnWSErrorCallback(string errorMessage);
    public delegate void OnWSMessageCallback(string message);

    public class AuvisusWebsocketClient
    {
        private Thread _socketThread;
        private string connectionString;
        private ClientWebSocket socket;

        private OnWSMessageCallback onWSMessageCallback;
        private OnWSErrorCallback onWSErrorCallback;
        private OnWSConnectCallback onWSConnectCallback;

        public AuvisusWebsocketClient(
            string connectionString, 
            OnWSMessageCallback onWSMessageCallback, 
            OnWSErrorCallback onWSErrorCallback,
            OnWSConnectCallback onWSConnectCallback
            )
        {
            this.connectionString = connectionString;
            this.onWSMessageCallback = onWSMessageCallback;
            this.onWSErrorCallback = onWSErrorCallback;
            this.onWSConnectCallback = onWSConnectCallback;
        }

        public void Start()
        {
            _socketThread = new Thread(SocketThreadFuncAsync);
            _socketThread.Start();
        }

        public void SendExternal(string data)
        {
            // Use this method to send data over the websocket
            if (socket != null && socket.State == WebSocketState.Open)
            {
                socket.SendAsync(Encoding.UTF8.GetBytes(data), WebSocketMessageType.Text, true, CancellationToken.None);
            }
            else
            {
                MessageBox.Show("Not connected");
            }
        }

        private async void SocketThreadFuncAsync(object state)
        {
            do
            {
                using (socket = new ClientWebSocket())
                {
                    try
                    {
                        await socket.ConnectAsync(new Uri(connectionString), CancellationToken.None);
                        onWSConnectCallback($"Connected to {connectionString}");

                        await Receive(socket);

                    }
                    catch (Exception ex)
                    {
                        onWSErrorCallback($"ERROR - {ex.Message}");
                    }
                }
            } while (true);
        }
        
        private async Task Send(ClientWebSocket socket, string data) =>
            await socket.SendAsync(Encoding.UTF8.GetBytes(data), WebSocketMessageType.Text, true, CancellationToken.None);

        private async Task Receive(ClientWebSocket socket)
        {
            var buffer = new ArraySegment<byte>(new byte[2048]);
            do
            {
                WebSocketReceiveResult result;
                using (var ms = new MemoryStream())
                {
                    do
                    {
                        result = await socket.ReceiveAsync(buffer, CancellationToken.None);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    } while (!result.EndOfMessage);

                    if (result.MessageType == WebSocketMessageType.Close)
                        break;

                    ms.Seek(0, SeekOrigin.Begin);
                    using (var reader = new StreamReader(ms, Encoding.UTF8))
                    {
                        onWSMessageCallback(await reader.ReadToEndAsync());
                    }
                        
                }
            } while (true);
        }

    }
}
