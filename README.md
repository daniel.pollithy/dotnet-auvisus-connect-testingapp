# dotNET-auvisus-connect-testingapp

This repository contains the code for a minimalistic WPF GUI which is capable of sending and receiving websocket messages to the auvisus websocket server.

## Setup

- Download, unzip and run the latest release of the dotNET auvisus connect testingapp: [dotNET auvisus connect testingapp v0.1.0](https://drive.google.com/file/d/1EDexJenkyT6KO3IbKrzhJ1g1TpL0CxyR/view?usp=sharing)
- Download and run the latest release of the javascript **auvisus connect testingapp**
   - This should open two windows: 
       - "visioncheckout Client": This implements the websocket server comparable to the real vision**checkout**.
       - "Cashier system client": This implements all necessary client messages to communicate with the server.
   - Please close the window "Cashier system client" since this is replaced by our dotNET testingapp

You can now implement your own websocket client, integrate it into the cashier PC and test its endpoints using the "visioncheckout Client".

## How should it look like?

![demo gif](2021-07-02-15-04-17.gif)
